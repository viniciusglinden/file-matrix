

# Generate Documentation

```sh
cd doc
doxygen
<BROWSER> doc.html
```

# Generate binary

```sh
cd build
cmake -S .. -B .
make
./file_matrix_manipulation <FILE.csv>
./file_matrix_manipulation ../testset/input.csv # for running the given file
```

Or, if you want to see more details, just run `./build`.
The `main.h` file contains some debugging levels that I've used to developing
and left for maintainability.

Test cases (including `input.csv`) are now located in the `testset` directory.
Please consult `testset/DESCRIPTION.md` for the expected results.

To test if the files that are supposed to fail do fail:

```sh
cd build
./run_tests
```

## Specification

CSV file that contains for now only signed decimal integer numbers in the
interval of [-99999999999;99999999999] **i.e. integer type is not sufficient**.
Framework is supposed to do the following:

- Opens the file which path is given as argument i,e. file_matrix_manipulation.exe input.csv`

## Implementation

Script was implemented in two main `.cpp` files:

1. `main.cpp` containing the main loop
2. `csv_manipulation.cpp` defining the classes and methods

`csv_manipulation` files describe two classes: one for a parser, just to open
and access elements; another called `matrix` do manipulate (do math) in it.

`main.h` is only used for setting the desired debug level.

## Return codes

Error codes are defined in the `return_codes` header file.

## Todos

Just type in the terminal:

```sh
grep -rni TODO
```

But also consider:

- use of inline functions
- use of `&` (alias) operator
- constant on reference
- implementing a move constructor
- change classes to use a template as type (containers)
- use more exception handling
- use Catch2 (like Google test/mockup) for extensive testing
- use fuzzy lop (like AFL) to discover bugs

## Assumptions

- number of columns is a fixed length, since this is usually done for computing
  acquired results from a measurement apparatus
- file will only contain numbers
- assuming that there will be no floating point numbers (`float` or `double`)

## Tools used

- `g++ v11.1.0 x86_64` (Artix Linux)
- `valgrind v3.17.0`
- neovim with coc for writing

