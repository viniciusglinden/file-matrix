/**
 * @file manipulation.cpp
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2021-06-03
 *
 * @brief Contains classes and functions for manipulating CSV data
 *
 */
#ifndef MANIPULATION_CPP__
#define MANIPULATION_CPP__

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>

#include "csv_manipulation.h"
#include "return_codes.h"
#include "colors.h"

namespace csv {

bool is_empty(std::ifstream& file)
{
	return file.peek() == std::ifstream::traits_type::eof();
}

parser::parser(std::string name) {
	parser::filename = name;
	int tmp = parser::input();
	if (tmp != NORMAL_EXIT) exit(tmp);
}

parser::parser(std::string name, char delimiter) {
	parser::delimiter = delimiter;
	parser::filename = name;
	int tmp = parser::input();
	if (tmp != NORMAL_EXIT) exit(tmp);
}

int parser::input() {
	int i = 0, column1, column2, *column; // crude column width check
	std::string line, element;

	std::ifstream file(parser::filename);
	if (!file.is_open()) {
		std::cerr << BOLDRED << "Error: Unable to open file" << RESETCOLOR <<std::endl;
		return ERR_FAILED_TO_OPEN;
	}

	if (is_empty(file)) {
		std::cerr << BOLDRED << "Error: empty file." << RESETCOLOR <<std::endl;
		return ERR_EMPTY_FILE;
	}

	// first read the whole line, then separate into elements,
	// this is one way to enable undetermined column width
	while(getline(file,line)) {
		if(line.empty()) continue;
		std::istringstream linestream(line);

		if (column == &column1) column = &column2;
		else column = &column1;

		*column = 0;
		while(getline(linestream,element,parser::delimiter)) {
			++(*column);
			try {
				parser::elements.push_back(stoll(element));
			} catch (...) {
				std::cerr << BOLDRED
					<< "Error: value conversion failed. Does the file contains a non-numeric character?"
					<< RESETCOLOR << std::endl;
				return ERR_NON_LONG_LONG;
			}
		}
		i++;
	}

	// if there is more than one line and column varies
	if (i > 1 && column1 != column2 ) {
		std::cerr << BOLDRED
			<< "Error: Column varies in width"
			<< RESETCOLOR << std::endl;
		return ERR_VARIES_WIDTH;
	}

	parser::column_width = *column;

	if (parser::elements.size() % parser::column_width) {
		std::cerr << BOLDRED
			<< "Error: Incomplete number of elements for column width, AKA missing elements!"
			<< RESETCOLOR << std::endl;
		return ERR_MISSING_ELEMENTS;
	}
	return NORMAL_EXIT;
}

void parser::error_outside_range_test(int n) {
	if (n < 0 || n > parser::elements.size()) {
		std::cerr << BOLDRED << "Error: tried to access element outside of range" << RESETCOLOR << std::endl;
		exit(ERR_OUTSIDE_RANGE);
	}
}

void parser::print() {
	std::cout << "File name: " << parser::filename << std::endl;
	std::cout << "Number of Elements: " << parser::elements.size() << std::endl;
	std::cout << "Column width: " << parser::column_width << std::endl;

	int i = 0;
	while (i < parser::elements.size()) {
		std::cout << parser::elements.at(i);
		if((++i) % parser::column_width)
			std::cout<<',';
		else
			std::cout<<std::endl;
	}
}

long long parser::element(int n) {
	parser::error_outside_range_test(n);
	return parser::elements[n];
}


long long parser::element(int line, int column) {
	int n = line*column_width + column;
	return parser::element(n);
}

int parser::get_length() {
	return parser::elements.size() / parser::column_width;
}

int parser::get_width() {
	return parser::column_width;
}

int parser::get_line(int n) {
	parser::error_outside_range_test(n);
	div_t tmp = div(n,parser::column_width);
	return tmp.quot;
}

int parser::get_column(int n) {
	parser::error_outside_range_test(n);
	return n % parser::column_width;
}

int parser::get_elements() {
	return parser::elements.size();
}

}

#endif /* MANIPULATION_CPP__ */
