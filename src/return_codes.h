/**
 * @file return_codes.h
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2021-06-05
 *
 * @brief Definition of return values
 *
 */

// TODO substitute some errors for exceptions (throw)
// TODO echo $? sometimes is not showing these error codes

#define NORMAL_EXIT				0	/// function exited without errors
#define ERR_NO_FILE_PROVIDED	55	/// user did not provide a file
#define ERR_FAILED_TO_OPEN		52	/// tried to open a file, but failed
#define ERR_EMPTY_FILE			51	/// empty file
#define ERR_MISSING_ELEMENTS	98	/// missing elements for column width
#define ERR_NON_LONG_LONG		239	/// file contains a non-long-long element
#define ERR_OUTSIDE_RANGE		112	/// tried to access element outside of range
#define ERR_VARIES_WIDTH		113	/// tried to access element outside of range
