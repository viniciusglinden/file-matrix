/**
 * @file main.h
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2021-06-05
 *
 * @brief Used for debugging
 *
 */

#ifndef MAIN_H__
#define MAIN_H__

/**
 * @defgroup DEBUG_LEVEL debugging options
 *
 * @{
 */

/**
 * UNDEFINED	no debug
 * 1			simple test parser
 */

#define DEBUG_LEVEL 1

/** @} */

#endif /* MAIN_H__ */
