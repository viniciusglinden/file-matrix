/**
 * @file manipulation.h
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2021-06-03
 *
 * @brief Contains classes and functions for manipulating CSV data
 *
 */

// TODO implement using a matrix in C++ to generate the file


#ifndef MANIPULATION_H__
#define MANIPULATION_H__

#include <vector>
#include <string>

namespace csv {

/**
 * @brief test is file is empty
 *
 * @return bool
 */
bool is_empty(std::ifstream& file);

/**
 * @brief CSV file parser
 *
 * @note no math can be performend. Element manipulation is future
 * implementation, since it is not necessary for now.
 */
class parser {
	protected:
		std::string filename; /// file name
		int column_width = 0; /// 1-based value column width
		char delimiter = ','; /// delimiter used
		std::vector<long long> elements; /// 1d array

		int input(); /// read file
		void error_outside_range_test(int n); /// test if number is within the elements range and exit with error if it doesn't
	public:
		/**
		 * @brief sets filename, delimiter = ',' and calls input()
		 *
		 * @param filename name of the CSV file
		 */
		parser(std::string filename);

		/**
		 * @brief sets filename, delimiter and calls input()
		 *
		 * @param filename name of the CSV file
		 */
		parser(std::string filename, char delimiter);

		/**
		 * @brief prints CSV content and file information
		 *
		 * @return void
		 */
		void print();

		/**
		 * @brief returns the element value
		 *
		 * @param n N'th 0-based element
		 *
		 * @return long long
		 */
		long long element(int n);

		/**
		 * @brief returns the element value
		 *
		 * @param line matrix 0-based line
		 * @param column matrix 0-based column
		 *
		 * @return long long
		 */
		long long element(int line, int column);

		/**
		 * @brief returns the 1-based column width
		 *
		 * @return int
		 */
		int get_width();

		/**
		 * @brief returns the 1-based number of lines
		 *
		 * @return int
		 */
		int get_length();

		/**
		 * @brief returns the 0-based line
		 *
		 * @param n element
		 *
		 * @return int
		 */
		int get_line(int n);

		/**
		 * @brief returns the 0-based column
		 *
		 * @param n element
		 *
		 * @return int
		 */
		int get_column(int n);

		/**
		 * @brief returns the 1-based number of elements
		 *
		 * @return int
		 */
		int get_elements();

		// TODO implement writing to elements
		//int wr_element(int n);
		//int wr_element(int line, int column);
		// TODO implement writing to file
		//int wr_file();
		//~parser();
};

}

#endif /* MANIPULATION_H__ */
