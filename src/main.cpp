/**
 * @file main.cpp
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2021-06-03
 *
 * @brief See README.md
 *
 */

#include "main.h"
#include "csv_manipulation.h"
#include "return_codes.h"
#include "colors.h"

#include <iostream>

int main(int argc, char *argv[]) {

	if(argc == 1) {
		std::cout << "Please provide a file!" << std::endl;
		std::cout << "Syntax:" <<  std::endl;
		std::cout << "\t" << argv[0] << " FILE" << std::endl;
		return ERR_NO_FILE_PROVIDED;
	}

#ifdef DEBUG_LEVEL
	std::cout << BOLDYELLOW << "DEBUG_LEVEL = " << DEBUG_LEVEL << RESETCOLOR << std::endl;
#endif

#if DEBUG_LEVEL == 1

	csv::parser parser(argv[1]);

	std::cout << "Column width: " << parser.get_width() << std::endl;
	std::cout << "Number of lines: " << parser.get_length() << std::endl;

	int k;
	for (k = 0; k < parser.get_elements(); ++k) {
		std::cout << "elements[" << k << "] = " << parser.element(k) << std::endl;
		std::cout << "elements[" << k << "]; line = " << parser.get_line(k) << std::endl;
		std::cout << "elements[" << k << "]; column = " << parser.get_column(k) << std::endl;
	}

	int line, column;
	for (line = 0; line < parser.get_length(); ++line)
		for (column = 0; column < parser.get_width(); ++column)
			std::cout << "element[" << line << "][" << column << "] = " << parser.element(line, column) << std::endl;

	parser.print();

#endif

	return NORMAL_EXIT;
}

